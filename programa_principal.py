'''Desarrollar un algoritmo que permita implementar un árbol como índice para hacer consultas de personajes de la saga de Star Wars, 
de los cuales se sabe su nombre, altura y peso. 
Además deberá contemplar los siguientes requerimientos (debe cargar al menos 20 personajes):
» a. se debe poder cargar un nuevo personaje, modificarlo (cualquiera de sus campos) y darlo de baja; 
» b. mostrar toda la información de Yoda, Mandalorian y Luke Skywalker
c. mostrar un listado ordenado alfabéticamente de los personajes que miden menos de 0.9 metro;
d. mostrar un listado ordenado alfabéticamente de los personajes que pesan mas de 75 kilos;
e. mostrar un listado por nivel de los personajes del árbol;
f. determinar si Grogu esta en el árbol y responder lo siguiente:

    mostrar toda su información;
    en que tipo de nodo esta (hoja, intermedio o raíz);
    que altura tiene el nodo dentro del árbol.



2 - Dado un grafo no dirigido con personajes de la saga Star Wars, implementar los algoritmos necesarios 
para resolver las siguientes tareas:a. cada vértice debe almacenar el nombre de un personaje, 
las aristas representan la cantidad de episodios en los que aparecieron juntos ambos personajes que se relacionan;
b. hallar el árbol de expansión mínimo desde el vértice que contiene a C-3PO, Yoda y la princesa Leia;
c. determinar cuales personajes comparten con otro dos episodios o mas (mostrar el par de pesonajes);
d. cargue al menos los siguientes personajes: Luke Skywalker, Darth Vader, Yoda, Boba Fett, C-3PO, Leia, Rey, Kylo Ren, Chewbacca, Han Solo, R2-D2, Obi-Wan kenobi; BB-8;
e. determinar que personaje es el que a compartido episodio con la mayor cantidad del resto de los personajes.'''


#información de los personajes de star wars obtenida de https://github.com/kerrybli/star-wars-characters-info
from dataclasses import dataclass


from grafo import Grafo
from arbol import  (
    arbolAcola,
    arbolAlista,
    
    nodoArbol,
    insertar_nodo,
    inorden_villano,
    inorden_empieza_con,
    contar_heroes,
    eliminar_nodo,
    inorden,
    por_nivel,
    postorden_heroes,
    crear_bosque,
    arbol_vacio,
    busqueda,
    contar_nodos
)

g = Grafo(False)
g.barrido_amplitud
@dataclass
class Personajes:
    nombre: None
    altura: float
    peso: float

#punto 1.a)
def cargarPersonaje(g, nombre, altura, peso):
    g.insertar_vertice(nombre, datos={'nombre':nombre, 'altura':altura, 'peso':peso})
def modificarPersonaje(g, buscado, nombre, altura, peso):
    vertice = g.busqueda_vertice(nombre, 'nombre')
    print('el siguiente personaje tiene las sig caracteristicas', vertice())
    g.eliminar_vertice_solo(vertice, 'nombre')
    print('vuelva a ingresar todos los datos')
    g.insertar_vertice(nombre, datos={'nombre':nombre, 'altura':altura, 'peso':peso})
def eliminar_personaje(g, nombre):
    g.eliminar_vertice(g.busqueda_vertice(nombre, 'nombre'))

def mostrar_yoda(g):
    print(g.obtener_elemento_vertice(g.busqueda_vertice('Yoda')))
    print(g.obtener_elemento_vertice(g.busqueda_vertice('Luke Skywalker')))
    #print(g.obtener_elemento_vertice(g.busqueda_vertice('Mandalorian')))
g.busqueda_vertice
# g.barrido_no_visitado()
#punto 1.c)
def buscar_Altura(g):
    
    print(g.busqueda_vertice_lista(90,'altura'))
g.obtener_elemento_vertice
# g.eliminar_vertice_solo()
# g.busqueda_vertice()
# g.obtener_elemento_vertice()


g.insertar_vertice('Luke Skywalker', datos={'nombre':'Luke Skywalker', 'altura':172, 'peso':72})
g.insertar_vertice('Darth Vader ',datos={'nombre':'Darth Vader', 'altura':202, 'peso':136})
g.insertar_vertice('Yoda', datos={'nombre':'Yoda', 'altura':'66', 'peso':'17'} )
g.insertar_vertice('Mandalorian', datos={})
g.insertar_vertice(' ' )
g.insertar_vertice(' ' )
g.insertar_vertice(' ' )
peso
g.insertar_arista('Luke Skywalker', 'Darth Vader', 2)

mostrar_yoda(g)
buscar_Altura(g)


#menu
# tecla = None
# while tecla!= 0:
#     print('')


#punto 1.d
#mostrar un listado ordenado alfabéticamente de los personajes que pesan mas de 75 kilos;

def buscar_por_peso(g, peso):
    listado = []
    listado = g.busqueda_vertice_mayor(peso, 'peso')
    for i in range(len(listado)):
        print(listado[i])

def buscar_por_altura(g, altura):
    listado = []
    listado = g.busqueda_vertice_menor(altura, 'altura')
    for i in range(len(listado)):
        print(listado[i])

buscar_por_peso(g, 75)
buscar_por_altura(90)
#1.e)
#determinar si Grogu esta en el árbol y responder lo siguiente:

    # mostrar toda su información;
    # en que tipo de nodo esta (hoja, intermedio o raíz);
    # que altura tiene el nodo dentro del árbol.
# a
def grogu(g=Grafo(),)
